-- La clause WHERE est évaluée AVANT tout calcul d'agrégation.
-- Si on tente:
SELECT service, AVG(salaire) moyenne
FROM employe
WHERE AVG(salaire) > 10000
GROUP BY service;
-- C'est impossible parce que WHERE filtre les enregistrements en ENTRÉE avant que la moyenne ne soit calculée.
-- HAVING filtre lui en sortie:
SELECT service, AVG(salaire) moyenne
FROM employe
GROUP BY service
HAVING moyenne > 10000;
-- Attention à l'ordre des clauses !
-- Attention, HAVING doit porter sur des colonnes sélectionnées.
-- Possible de combiner WHERE et HAVING:
SELECT service, AVG(salaire) moyenne
FROM employe
WHERE salaire > 12000
GROUP BY service
HAVING moyenne > 15000;
-- Et regarder bien la différence avec:
SELECT service, AVG(salaire) moyenne
FROM employe
WHERE salaire > 15000
GROUP BY service
HAVING moyenne > 15000;
-- Attention à la performance. Pour le même résultat, la requête:
SELECT sexe, AVG(salaire) moyenne
FROM employe
WHERE sexe = 'F'
GROUP BY sexe;
-- Est plus performante que la requête:
SELECT sexe, AVG(salaire) moyenne
FROM employe
GROUP BY sexe
HAVING sexe = 'F';
-- Ordre des clauses requêtes:
SELECT... FROM
WHERE
GROUP BY
HAVING
ORDER BY
LIMIT
