// En utilisant la base entreprise, écrivez les 10 requêtes qui répondent aux 10 questions.

// Questions pour cet exercice

// Sélectionnez le nom et le prénom de l'employé masculin qui gagne plus de 15000.

SELECT nom, prenom from employe WHERE sexe = 'M'
AND salaire > 15000

// Sélectionnez le prénom des 3 employés qui gagnent le plus. Tri par salaire descendant.

SELECT prenom FROM employe ORDER BY salaire DESC LIMIT 3;

// Sélectionnez le plus petit salaire aliasé en salaireMin.

SELECT salaire salaireMin
FROM employe
ORDER BY salaire
LIMIT 1;

// Sélectionnez les 4 noms différents des employés. Tri par nom ascendant.

SELECT DISTINCT nom FROM employe ORDER BY nom;

// Sélectionnez le salaire de l'employé qui n'a pas de service.

SELECT salaire FROM employe WHERE ISNULL(service);
// ou 
SELECT salaire FROM employe WHERE service IS NULL;

// Sélectionnez les noms et prénoms des employés triés par ancienneté, du plus ancien au plus récemment embauché.

SELECT nom, prenom FROM employe ORDER BY dateContrat;

// Sélectionnez les noms et prénoms des employées du service Marketing. Tri par nom puis prénom.

SELECT nom, prenom
FROM employe
WHERE service = 'Marketing'
ORDER BY nom, prenom;

// Sélectionnez le prénom du second employé gagnant le plus.

SELECT prenom
FROM employe
ORDER BY salaire DESC
LIMIT 1, 1;

// Sélectionnez le service de l'employé qui gagne 8500.

SELECT service
FROM employe
WHERE salaire = 8500;

// Sélectionnez le service dans lequel travaille l'employé qui gagne le plus.

SELECT service
FROM employe
ORDER BY salaire DESC
LIMIT 1;