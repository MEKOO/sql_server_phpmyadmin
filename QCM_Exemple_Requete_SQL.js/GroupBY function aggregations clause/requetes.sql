-- Et si on souhaite obtenir en une seule requête la moyenne des salaires par sexe ?
SELECT sexe, AVG(salaire) moyenne
FROM employe
GROUP BY sexe;
-- Voilà pour le bon usage. Alors plusieurs remarques:
---- Un GROUP BY sur une colonne ne figurant pas parmi les colonnes sélectionnées est INintéressant:
SELECT sexe, AVG(salaire) moyenne
FROM employe
GROUP BY service;
---- Un GROUP BY sans fonctions d'agrégation est INintéressant:
SELECT service, nom, prenom
FROM employe
GROUP BY service;
---- Parfois utilisé maladroitement pour dédoublonner:
SELECT nom
FROM employe
GROUP BY nom;
---- Préférez DISTINCT plus performant:
SELECT DISTINCT nom
FROM employe;
