// Questions pour cet exercice
// Sélectionnez le salaire moyen du service Marketing.Alias salaireMoyenMarketing.

SELECT AVG(salaire) salaireMoyenMarketing
FROM employe
WHERE service = 'Marketing';

// Sélectionnez le salaire moyen(arrondi à un entier) par service.Alias salaireMoyen.Tri par salaire moyen descendant.

SELECT service, ROUND(AVG(salaire))
salaireMoyen
FROM employe
GROUP BY service
ORDER BY salaireMoyen DESC

// Idem - 2 - mais sans le service NULL.
SELECT service, ROUND(AVG(salaire)) salaireMoyen
FROM employe
WHERE service IS NOT NULL
GROUP BY service
ORDER BY salaireMoyen DESC;

// Sélectionnez le nombre d 'employés par service. Alias nb. Tri par nombre descendant puis service ascendant.
SELECT service, COUNT( * ) nb
FROM employe
GROUP BY service
ORDER BY nb DESC, service;

// Idem - 4 - mais sans le service NULL.
SELECT service, COUNT( * ) nb
FROM employe
WHERE service IS NOT NULL
GROUP BY service
ORDER BY nb DESC, service;

// Idem - 5 - mais uniquement pour les employés gagnant moins de 15000.
SELECT service, COUNT( * ) nb
FROM employe
WHERE service IS NOT NULL AND salaire < 15000
GROUP BY service
ORDER BY nb DESC, service;
// Idem - 5 - mais uniquement si le nombre est au moins 2.
SELECT service, COUNT( * ) nb
FROM employe
WHERE service IS NOT NULL
GROUP BY service
HAVING nb >= 2
ORDER BY nb DESC, service;
// Pour chaque nom de famille, sélectionnez le nombre d 'employés le portant. Alias nb. Tri par nom ascendant.
SELECT nom, COUNT(nom) nb
FROM employe
GROUP BY nom
ORDER BY nom;
// Idem - 8 - mais uniquement pour les employés dont le nom commence par Du.
SELECT nom, COUNT(nom) nb
FROM employe
WHERE nom LIKE 'Du%'
GROUP BY nom
ORDER BY nom;
// Idem - 9 - mais uniquement si le nombre est au moins 2.
SELECT nom, COUNT(nom) nb
FROM employe
WHERE nom LIKE 'Du%'
GROUP BY nom
HAVING nb >= 2
ORDER BY nom;