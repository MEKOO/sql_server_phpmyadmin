-- COUNT(col) ne dénombre pas les NULL:
SELECT COUNT(service) nbServices FROM employe;
-- COUNT(*) dénombre les NULL:
SELECT COUNT(*) nbEmployes FROM employe;
-- MIN(), MAX():
SELECT MIN(salaire) salaireMin FROM employe;
-- Avec les nombres, les chaînes, les dates:
SELECT MAX(nom) nomMax FROM employe;
-- Ou:
SELECT MAX(dateContrat) dateContratMax FROM employe;
-- SUM():
SELECT SUM(salaire) totalSalaire FROM employe;
-- Calculer la moyenne ?
SELECT SUM(salaire) / COUNT(*) moyenne FROM employe;
-- Plus simple avec AVG():
SELECT AVG(salaire) moyenne FROM employe;
